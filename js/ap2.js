// const { default: axios } = require("axios")
const formulario = document.getElementById('formulario');
const res = document.getElementById('res');
formulario.addEventListener('submit', e => {
    e.preventDefault();
    const valor = document.getElementById('valor').value;
    llamadaConAxios(valor);
})


const llamadaConAxios = async (user) => {
    try {
        const respuesta = await axios(`https://jsonplaceholder.typicode.com/users/${user}`);
        mostrar(respuesta);
    } catch (error) {
        eliminar()
        const h1 = document.createElement('h1')
        h1.innerText = 'No se encontraron los datos';
        res.appendChild(h1)
    }

}
function mostrar(datos) {
    eliminar()
    const h1 = document.createElement('h1')
    const h2 = document.createElement('h1')
    const h3 = document.createElement('h1')
    const h4 = document.createElement('h1')
    const h5 = document.createElement('h1')
    const h6 = document.createElement('h1')
    h1.innerText = datos.data.name;
    h2.innerText = datos.data.username;
    h3.innerText = datos.data.email;
    h4.innerText = datos.data.address.street;
    h5.innerText = datos.data.address.suite
    h6.innerText = datos.data.address.city;

    res.appendChild(h1)
    res.appendChild(h2)
    res.appendChild(h3)
    res.appendChild(h4)
    res.appendChild(h5)
    res.appendChild(h6)

}

function eliminar() {
    while (res.firstChild) {
        res.removeChild(res.firstChild);
    }
}
